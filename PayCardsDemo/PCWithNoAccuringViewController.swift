//
//  PCWithNoAccuringViewController.swift
//  PayCardsDemo
//
//  Created by Dmitriy Bagrov on 17.11.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

import UIKit
import PayCardsNoAcquiring

class PCWithNoAccuringViewController: UIViewController, PCDCardScanViewControllerDelegete {
    
    // MARK: - Properies
    
    var merchant: PCDMerchant {
        let _merchant = PCDMerchant()
        _merchant.ID = 100333
        _merchant.password = "zI02NS3c"
        _merchant.clientId = "777"
        _merchant.email = "dmitriy.bagrov@walletone.com"
        _merchant.phone = "79788014244"
        return _merchant
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presentPayCards()
    }

    // MARK: - Present Methods
    
    func presentPayCards() {
        let scanVC = PCDCardScanViewController()
        scanVC.merchant = merchant
        scanVC.pcdDelegate = self
        
        navigationController?.pushViewController(scanVC, animated: true)
    }
    
    // MARK: - PCDCardScanViewControllerDelegete
    
    func cardScanViewController(cardScanViewController: PCDCardScanViewController!, didCloseWithError error: NSError!) {
        
    }
    
    func cardScanViewController(cardScanViewController: PCDCardScanViewController!, didFinishScanWithInfo cardInfo: [NSObject : AnyObject]!) {
        
    }
    
    func userDidCloseCardScanViewController(cardScanViewController: PCDCardScanViewController!) {
        
    }
}

