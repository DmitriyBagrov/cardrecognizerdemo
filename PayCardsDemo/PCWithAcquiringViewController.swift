//
//  PCWithAcquiringViewController.swift
//  PayCardsDemo
//
//  Created by Dmitriy Bagrov on 25.11.15.
//  Copyright © 2015 Dmitriy Bagrov. All rights reserved.
//

import UIKit
import PayCardsAcquiringObjC

class PCWithAcquiringViewController: UIViewController, PCDPaymentViewControllerDelegete {
    
    // MARK: - Properies
    
    var examplePayment: PCDPayment {
        let payment = PCDPayment()
        payment.orderDescription = "Example Payment"
        payment.amount = 0.2
        payment.currencyCode = .USD
        
        payment.orderNumber = "1" //Oder number must be unique
        
        let customer = PCDCustomer()
        customer.ID = "12345"
        customer.email = "customer@email.com"
        customer.phoneNumber = "9998880000"
        customer.firstname = "John"
        customer.lastname = "Smith"
        payment.customer = customer
        
        return payment
    }

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurePayCards()
        
        openPayCardsPaymentViewController()
    }
    
    // MARK: - Configure Methods
    
    private func configurePayCards() {
        PCDConfiguration.sharedInstance().merchantId = "117223428187"
        PCDConfiguration.sharedInstance().culture = .EN
        PCDConfiguration.sharedInstance().soundEnabled = true
        PCDConfiguration.sharedInstance().saveCard = true
        PCDConfiguration.sharedInstance().storeCVV = true
    }
    
    // MARK: - Present method
    
    private func openPayCardsPaymentViewController() {
        let paymentVC = PCDPaymentViewController()
        paymentVC.payment = examplePayment
        
        paymentVC.presentOnViewController(self.navigationController, delegate: self) { () -> Void in }
        
    }
    
    // MARK: - PCDPaymentViewControllerDelegete
    
    func paymentViewController(paymentViewController: PCDPaymentViewController!, didCompletePayment payment: PCDPayment!, transactionId: String!, error: NSError!) {
        
    }
    
    func paymentViewController(paymentViewController: PCDPaymentViewController!, didCancelPayment payment: PCDPayment!) {
        
    }
    
    func paymentViewController(paymentViewController: PCDPaymentViewController!, didGetProccesingStateForPayment payment: PCDPayment!, transactionId: String!, error: NSError!) {
        
    }
}
