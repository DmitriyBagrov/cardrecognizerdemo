//
//  CRNPaymentViewController.h
 
#import <UIKit/UIKit.h>

@class PCDPayment, PCDPaymentViewController;

@protocol PCDPaymentViewControllerDelegete <NSObject>
@optional

- (void)paymentViewController:(PCDPaymentViewController *)paymentViewController
           didCompletePayment:(PCDPayment *)payment
                transactionId:(NSString *)transactionId
                        error:(NSError *)error;

- (void)paymentViewController:(PCDPaymentViewController *)paymentViewController didCancelPayment:(PCDPayment *)payment;

- (void)paymentViewController:(PCDPaymentViewController *)paymentViewController
didGetProccesingStateForPayment:(PCDPayment *)payment
                transactionId:(NSString *)transactionId
                        error:(NSError *)error;

@end

@interface PCDPaymentViewController : UINavigationController

@property (nonatomic) PCDPayment *payment;


- (void)presentOnViewController:(UIViewController *)viewController
                       delegate:(id<PCDPaymentViewControllerDelegete>)delegate
                     completion:(void (^)(void))completion;

- (void)dismissWithCompletion:(void (^)(void))completion;

@end
