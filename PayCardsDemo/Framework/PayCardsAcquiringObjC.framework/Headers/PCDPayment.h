//
//  CRNPayment.h
 
#import <Foundation/Foundation.h>

typedef NS_ENUM(int, PCDCurrencyCode) {
    PCDCurrencyCodeRUB = 643,
    PCDCurrencyCodeKZT = 398,
    PCDCurrencyCodeZAR = 710,
    PCDCurrencyCodeUSD = 840,
    PCDCurrencyCodeTJS = 972,
    PCDCurrencyCodeBYR = 974,
    PCDCurrencyCodeEUR = 978,
    PCDCurrencyCodeUAH = 980,
    PCDCurrencyCodeGEL = 981,
    PCDCurrencyCodePLN = 985
};

@class PCDCustomer;

@interface PCDPayment : NSObject

@property (nonatomic, copy) NSString *orderNumber;
@property (nonatomic, copy) NSString *orderDescription;
@property (nonatomic, copy) NSNumber *amount;
@property (nonatomic) PCDCurrencyCode currencyCode;
@property (nonatomic) PCDCustomer *customer;

@end
