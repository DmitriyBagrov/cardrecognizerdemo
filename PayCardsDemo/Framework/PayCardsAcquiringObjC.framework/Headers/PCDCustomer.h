//
//  CRNCustomer.h
 
#import <Foundation/Foundation.h>

@interface PCDCustomer : NSObject

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *firstname;
@property (nonatomic, copy) NSString *lastname;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *phoneNumber;

@end
