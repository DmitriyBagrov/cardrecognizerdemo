//
//  CRNTransactionHistory.h
 
#import <Foundation/Foundation.h>

typedef NS_ENUM(int, PCDPaymentTransactionStatus)  {
    PCDPaymentTransactionStatusUnknown,
    PCDPaymentTransactionStatusCreated,
    PCDPaymentTransactionStatusAuthRequired,
    PCDPaymentTransactionStatusPending,
    PCDPaymentTransactionStatusSuccess,
    PCDPaymentTransactionStatusError,
};

@interface PCDTransactionHistory : NSObject

+ (void)requestTransactionStatusByTransactionId:(NSString *)transactionId
                                     completion:(void(^)(PCDPaymentTransactionStatus status, NSError *error))completion;

@end
