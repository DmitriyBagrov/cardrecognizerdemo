//
//  CRNConfiguration.h
 
#import <Foundation/Foundation.h>
#import "PCDConfigurationSettings.h"

typedef NS_ENUM(NSInteger, PCDCulture) {
    PCDCultureEN,
    PCDCultureRU,
    PCDCultureES,
    PCDCultureFR,
    PCDCultureIT,
    PCDCulturePT,
    PCDCultureUA,
    PCDCultureZH
};

@interface PCDConfiguration : NSObject

@property (nonatomic) PCDCulture culture;
@property (nonatomic, copy) NSString *merchantId;
@property (nonatomic, getter=isSoundEnabled) BOOL soundEnabled;
@property (nonatomic, assign) BOOL saveCard;
@property (nonatomic, assign) BOOL storeCVV;
@property (nonatomic) PCDRequiredCardFields requeriedCardFields;
@property (nonatomic, copy) NSString *processingServerUrl;

+ (instancetype)sharedInstance;


@end
