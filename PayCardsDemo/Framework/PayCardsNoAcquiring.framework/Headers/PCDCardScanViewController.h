//
//  PCDCardScanViewController.h
//  CardRecognition
//
//  Created by Igor Pesin on 2/10/15.
//  Copyright (c) 2015 PayCards. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PCDCardScanViewController, PCDMerchant;

/// PCDCardScanViewController delegate
@protocol PCDCardScanViewControllerDelegete <NSObject>
@optional

/**
 Fires when card recognition finishes successfully
 
 @note Card Info example:
 {
    cardParams =     {
        cardName = My Card;
        expireDate = "2015-09-01T00:00:00";
        holderName = John Doe;
        number = 5402341040016512;
    };
    scanId = 113;
 }
 
 @param scanId scanId parameter is used to report if recognized data is correct or not
 */
- (void)cardScanViewController:(PCDCardScanViewController *)cardScanViewController didFinishScanWithInfo:(NSDictionary *)cardInfo;

/// Fires when error occurs
- (void)cardScanViewController:(PCDCardScanViewController *)cardScanViewController didCloseWithError:(NSError *)error;

/// Fires when user interrupts recognition tapping Back button
-(void)userDidCloseCardScanViewController:(PCDCardScanViewController *)cardScanViewController;

@end


/// Root Card Scan View Controller
@interface PCDCardScanViewController: UIViewController

/// PCDCardScanViewController delegate
@property (nonatomic, weak) id<PCDCardScanViewControllerDelegete> pcdDelegate;

/// Merchant property is necessary, otherwise error will occur
@property (nonatomic, strong) PCDMerchant *merchant;

@end
