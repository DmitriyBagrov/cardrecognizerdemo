//
//  PCDConfigurationManager.h
//  CardRecognition
//
//  Created by Igor Pesin on 12/4/14.
//  Copyright (c) 2014 PayCards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PCDConfigurationSettings.h"

/** 
 Framework localization
 
 @param PCDLocalizationEN English locale
 @param PCDLocalizationRU Russian locale
 */
typedef NS_ENUM(NSInteger, PCDLocalization) {
    PCDLocalizationEN,
    PCDLocalizationRU
};

/// Allows user to configure framework. Includes the following settings
@interface PCDConfigurationManager: NSObject

/// Framework localization
@property (nonatomic) PCDLocalization localization;

/// SoundEnabled. YES by default.
@property (nonatomic, getter=isSoundEnabled) BOOL soundEnabled;

/// Indicate if user will be asked to enter the CVV code after scan. YES by default.
@property (nonatomic, assign) BOOL collectCVV;

/// Indicate which fields (holder name, expiration date, card name) of the recognized card should be shown after the recognition. Default value is PCDRequiredCardFieldAll.
@property (nonatomic) PCDRequiredCardFields requeriedCardFields;

/// Indicate if a recognized card will be saved. Cards's data is stored in iOS Keychain and can be used by the client with the same ClientId for future payments. YES by default.
@property (nonatomic, assign) BOOL saveCard;

/// Indicate if a card’s CVV will be stored in iOS Keychain. For future payments, saved CVV can be obtained after entering passcode or Touch ID. YES by default.
@property (nonatomic, assign) BOOL storeCVV;

+ (instancetype)sharedInstance;

@end


